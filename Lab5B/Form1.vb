﻿Option Strict On

''' <Assignment>
''' <course> IS116 - Introduction to Computer Programming (VB)</course>
''' <Author> Brian Davis</Author>
''' <Project>Lab5B </Project>
''' <Date> May 19,2014</Date>
''' </Assignment>
Public Class Form1


''' <summary>
''' 1. User Clicks on Compare Options Button
''' 
''' 2. For Loop runs, first checking with an IF Statement to see if it is the first loop "1st Day"
'''    If it is the first loop "1st Day" then Don't double the salary
'''    If it is after the first loop IndexDay > 1 then double the salary
''' 
''' 3. After Loop complete its loops it then displays the Option Totals to the User. 
''' </summary>
''' <param name="sender">Standard (IDE-supplied)</param>
''' <param name="e">User Clicks on Compare Options Button </param>
''' <remarks></remarks>
Private Sub CompareOptionsButton_Click(sender As Object, e As EventArgs) Handles CompareOptionsButton.Click

	Dim TotalDays As Integer = 10			  'Total Days to compare options with  {10 Days}

	Dim Option1Rate As Decimal = 1D			  'First  Option Per Day Rate			{$1.00}  
	Dim Option2Rate As Decimal = 100D		  'Second Option Per Day Rate			{$100.00}

	Dim Option1TotalSalary As Decimal = 0	  'First  Option Total Salary
	Dim Option2TotalSalary As Decimal = 0	  'Second Option Total Salary

	Dim Salary As Decimal = 0				  'Holding variable for TotalSalary 

	'Loops through TotalDays being compared
	For IndexDay As Integer = 1 To TotalDays  'Uses TotalDays for upper end, Having variable instead of hard coded number allows program in future to accept this
											  'number as input from the user instead of having it hard coded like it is in the current state.

			If IndexDay = 1 Then			  'On First Loop Salary = Option1Rate   {1st Day = $1.00}  Doubling don't take effect yet. 
				Salary = Option1Rate		  'Having [Salary = Option1Rate] gives option in future program to switch out if comparing with options that are not flat rate 
			Else
				Salary *= 2					  'Loops after First Loop doubling takes effect  {2nd Day = $1.00 * 2 = $2.00}  {3rd Day = $2.00 * 2 = $4.00}
			End If

			Option1TotalSalary += Salary	  'Keeps a running total of Total Salary

	Next IndexDay

		Option2TotalSalary = (Option2Rate * TotalDays)	  'Calculates Option Two Total Salary {in this case its a flat rate no need for a loop}

	'Displays the Totals in their Display Labels in Currency format
		Me.Option1DisplayLabel.Text = Option1TotalSalary.ToString("c")
		Me.Option2DisplayLabel.Text = Option2TotalSalary.ToString("c")


End Sub


''' <summary>
''' Exits the Program when User clicks on the Exit Button
''' </summary>
''' <param name="sender">Standard (IDE-supplied)</param>
''' <param name="e"> User Clicks on Exit Button</param>
''' <remarks></remarks>
Private Sub ExitButton_Click(sender As Object, e As EventArgs) Handles ExitButton.Click

Me.Close()

End Sub



End Class
