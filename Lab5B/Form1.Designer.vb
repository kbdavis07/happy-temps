﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.Option1Label = New System.Windows.Forms.Label()
		Me.Option1DisplayLabel = New System.Windows.Forms.Label()
		Me.Option2Label = New System.Windows.Forms.Label()
		Me.Option2DisplayLabel = New System.Windows.Forms.Label()
		Me.CompareOptionsButton = New System.Windows.Forms.Button()
		Me.ExitButton = New System.Windows.Forms.Button()
		Me.SuspendLayout()
		'
		'Option1Label
		'
		Me.Option1Label.Location = New System.Drawing.Point(13, 61)
		Me.Option1Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Option1Label.Name = "Option1Label"
		Me.Option1Label.Size = New System.Drawing.Size(452, 21)
		Me.Option1Label.TabIndex = 0
		Me.Option1Label.Text = "Option: 1  (Starts at $1 and then doubles each day for 10 days):  "
		'
		'Option1DisplayLabel
		'
		Me.Option1DisplayLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Option1DisplayLabel.Location = New System.Drawing.Point(464, 61)
		Me.Option1DisplayLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Option1DisplayLabel.Name = "Option1DisplayLabel"
		Me.Option1DisplayLabel.Size = New System.Drawing.Size(148, 21)
		Me.Option1DisplayLabel.TabIndex = 1

		'
		'Option2Label
		'
		Me.Option2Label.AutoSize = True
		Me.Option2Label.Location = New System.Drawing.Point(191, 91)
		Me.Option2Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Option2Label.Name = "Option2Label"
		Me.Option2Label.Size = New System.Drawing.Size(265, 21)
		Me.Option2Label.TabIndex = 2
		Me.Option2Label.Text = "Option 2: ($100 per day for 10 days):"
		'
		'Option2DisplayLabel
		'
		Me.Option2DisplayLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Option2DisplayLabel.Location = New System.Drawing.Point(464, 91)
		Me.Option2DisplayLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Option2DisplayLabel.Name = "Option2DisplayLabel"
		Me.Option2DisplayLabel.Size = New System.Drawing.Size(148, 21)
		Me.Option2DisplayLabel.TabIndex = 3

		'
		'CompareOptionsButton
		'
		Me.CompareOptionsButton.BackColor = System.Drawing.Color.MediumSeaGreen
		Me.CompareOptionsButton.ForeColor = System.Drawing.SystemColors.ControlLight
		Me.CompareOptionsButton.Location = New System.Drawing.Point(341, 132)
		Me.CompareOptionsButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.CompareOptionsButton.Name = "CompareOptionsButton"
		Me.CompareOptionsButton.Size = New System.Drawing.Size(156, 37)
		Me.CompareOptionsButton.TabIndex = 4
		Me.CompareOptionsButton.Text = "&Compare Options"
		Me.CompareOptionsButton.UseVisualStyleBackColor = False
		'
		'ExitButton
		'
		Me.ExitButton.BackColor = System.Drawing.Color.Firebrick
		Me.ExitButton.ForeColor = System.Drawing.SystemColors.ControlLightLight
		Me.ExitButton.Location = New System.Drawing.Point(527, 132)
		Me.ExitButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.ExitButton.Name = "ExitButton"
		Me.ExitButton.Size = New System.Drawing.Size(89, 37)
		Me.ExitButton.TabIndex = 5
		Me.ExitButton.Text = "E&xit"
		Me.ExitButton.UseVisualStyleBackColor = False
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 21.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(676, 216)
		Me.Controls.Add(Me.ExitButton)
		Me.Controls.Add(Me.CompareOptionsButton)
		Me.Controls.Add(Me.Option2DisplayLabel)
		Me.Controls.Add(Me.Option2Label)
		Me.Controls.Add(Me.Option1DisplayLabel)
		Me.Controls.Add(Me.Option1Label)
		Me.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.Name = "Form1"
		Me.Text = "Happy Temps"
		Me.ResumeLayout(False)
		Me.PerformLayout()

End Sub
	Friend WithEvents Option1Label As System.Windows.Forms.Label
 Friend WithEvents Option1DisplayLabel As System.Windows.Forms.Label
 Friend WithEvents Option2Label As System.Windows.Forms.Label
 Friend WithEvents Option2DisplayLabel As System.Windows.Forms.Label
 Friend WithEvents CompareOptionsButton As System.Windows.Forms.Button
 Friend WithEvents ExitButton As System.Windows.Forms.Button

End Class
